#  小象电商B2B2C 微信小程序端开源

#### 演示环境
商城小程序端：
- ![输入图片说明](https://images.gitee.com/uploads/images/2021/0225/175527_375a7b51_5325125.jpeg "xiaoxiangweicha.png")


####小象B2B2C电商演示地址：

PC：https://shop.weixinai.cn/ jack/123456

商户端：https://shop.weixinai.cn/business  business/123456

后台管理：https://shop.weixinai.cn/admin admin/123456


#### 介绍
小象B2B2C电商的微信小程序端开源，为您提供B2B2C的小程序电商快速开发和交付。
 
** 小象B2B2C电商微信小程序源代码完全开源并且免费，如获取源码后台请点赞+收藏，
     添加管理员微信发截图即可获取，谢谢! ** 

1. 小象智慧微信：yubang1010
- ![输入图片说明](images/navImg/200.png)

#### 官网

1. 官网地址：https://www.xiaoxiangai.com

#### 软件架构
1. 核心框架：Spring Boot 2.0.3.RELEASE
2. 安全框架：Apache Shiro 1.4.0
3. 视图框架：Spring MVC 5.0.6
4. 搜索框架：Elasticsearch 6.5.0

#### 推荐运行环境
1. 操作系统：Linux、Unix、Windows
2. JDK：JDK 1.8
3. 应用服务器：Tomcat 8.5
4. 数据库：MySQL 5.7 +
5. Redis：6.0
6. Elasticsearch：6.5
7. 项目运行
- 下载微信小程序开发者工具：https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html
- 下载并导入本商城小程序代码
- 小程序编译并运行

#### 后台服务代码下载地址
链接: https://pan.baidu.com/s/1T_IHZw1ozJSQ_8yEz6XjAg?pwd=xibh 
提取码: xibh 

#### 有些同学反馈JAR下载不了，可以点下面的地址下载
链接：https://pan.baidu.com/s/1-zko8IXLLwylXrWJg5r3fQ?pwd=2mf9 
提取码：2mf9

#### 小象B2B2C开源电商商城操作培训
https://www.bilibili.com/video/BV1aN411Z7Rx/?spm_id_from=333.337.search-card.all.click&vd_source=2b01ecb1d6f4547c5d37977d201a45b8
